package com.example.bstask.network.api

import androidx.annotation.Keep
import com.example.bstask.common.constants.ApiConstants
import com.example.bstask.models.BookList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

@Keep
interface BookApi {
    @GET(ApiConstants.API_BOOK_LIST)
    suspend fun getBookList() : Response<BookList>

}