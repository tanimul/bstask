package com.example.bstask.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class BookList(

    @SerializedName("status") var status: Int? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("data") var data: ArrayList<BookInfo> = arrayListOf()

)