package com.example.bstask.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class BookInfo(

    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("image") var image: String? = null,
    @SerializedName("url") var url: String? = null

)