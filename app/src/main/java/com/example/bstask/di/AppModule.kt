package com.example.bstask.di

import android.content.Context
import android.net.ConnectivityManager
import androidx.annotation.Keep
import com.example.bstask.common.constants.AppConstants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named


@Module
@InstallIn(SingletonComponent::class)
@Keep
object AppModule {
    @Provides
    @Named("base_url")
    fun providesBaseUrl(): String {
        return AppConstants.API_URL_DEV
    }

    @Provides
    fun provideConnectivityManager(@ApplicationContext context: Context): ConnectivityManager {
        return context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }
}