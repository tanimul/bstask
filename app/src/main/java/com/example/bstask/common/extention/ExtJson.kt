package com.example.bstask.common.extention

import com.google.gson.Gson

fun Any.toJson(): String? {
    return Gson().toJson(this)
}
