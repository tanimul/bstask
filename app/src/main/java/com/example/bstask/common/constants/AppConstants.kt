package com.example.bstask.common.constants

object AppConstants {

    const val API_ACCEPT = "Accept"
    const val API_ACCEPT_VALUE = "application/json"

    const val API_URL_DEV: String = "https://example.com/"


    const val PACKAGE_ID = "package-id"
    const val PACKAGE = "com.example.bstask"
}