package com.example.bstask.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.bstask.base.BaseRecyclerAdapterView
import com.example.bstask.databinding.ItemBookBinding
import com.example.bstask.models.BookInfo
import com.example.bstask.models.BookList

class BookListAdapter (private val onRepoClicked : (BookInfo?) -> Unit) : BaseRecyclerAdapterView<ItemBookBinding, BookInfo>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        return ViewHolder(
            ItemBookBinding.inflate(
                LayoutInflater.from(mContext),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = holder.binding
        val data = data[position]

        data.apply {
            binding.bookInfo = this
        }

        binding.root.setOnClickListener {
            onRepoClicked(data)
        }
    }

}