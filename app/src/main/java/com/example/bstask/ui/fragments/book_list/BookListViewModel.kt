package com.example.bstask.ui.fragments.book_list

import androidx.lifecycle.ViewModel
import com.example.bstask.models.BookList
import com.example.bstask.network.core.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber
import javax.inject.Inject


@HiltViewModel
class BookListViewModel @Inject constructor(private  val repo : BookListRepository) : ViewModel() {

    fun getBookList() : Flow<Resource<BookList>> {
        Timber.d("getBookList:")
        return  repo.getBookList().flowOn(Dispatchers.IO)
    }
}