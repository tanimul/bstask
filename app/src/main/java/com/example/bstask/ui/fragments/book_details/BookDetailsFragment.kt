package com.example.bstask.ui.fragments.book_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.bstask.R
import com.example.bstask.base.BaseFragment
import com.example.bstask.common.extention.checkIsEmpty
import com.example.bstask.databinding.FragmentBookDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : BaseFragment<FragmentBookDetailsBinding>() {


    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBookDetailsBinding= DataBindingUtil.inflate(
        layoutInflater, R.layout.fragment_book_details, container, false
    )

    override fun init() {

        // Retrieve the argument
        val repoDetails = arguments?.getString("book_details")

        if(repoDetails?.checkIsEmpty() == false){
            binding.details = repoDetails
        }


    }

}
