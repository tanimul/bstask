package com.example.bstask.ui.fragments.book_list

import android.net.ConnectivityManager
import com.example.bstask.models.BookInfo
import com.example.bstask.models.BookList
import com.example.bstask.network.api.BookApi
import com.example.bstask.network.core.Resource
import com.example.bstask.network.core.networkCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class BookListRepository @Inject constructor(
    private val connectivityManager: ConnectivityManager,
    private val api : BookApi
){

    fun getBookList() : Flow<Resource<BookList>> {
        return networkCall(connectivityManager){
            api.getBookList()
        }.flowOn(Dispatchers.IO)
    }

}