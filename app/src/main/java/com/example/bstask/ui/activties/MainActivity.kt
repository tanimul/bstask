package com.example.bstask.ui.activties

import android.os.Bundle
import com.example.bstask.base.BaseActivity
import com.example.bstask.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(){

    override fun getViewBinding(): ActivityMainBinding {
        return  ActivityMainBinding.inflate(layoutInflater)
    }

    override fun init(savedInstanceState: Bundle?) {
        //do initial work here
    }
}