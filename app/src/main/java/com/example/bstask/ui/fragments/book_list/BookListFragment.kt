package com.example.bstask.ui.fragments.book_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bstask.R
import com.example.bstask.adapter.BookListAdapter
import com.example.bstask.base.BaseFragment
import com.example.bstask.common.extention.snackBarError
import com.example.bstask.databinding.FragmentBookListBinding
import com.example.bstask.models.BookInfo
import com.example.bstask.network.core.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import timber.log.Timber

@AndroidEntryPoint
class BookListFragment : BaseFragment<FragmentBookListBinding>() {

    private val viewModel: BookListViewModel by viewModels()
    private var bookListAdapter: BookListAdapter? = null


    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBookListBinding = DataBindingUtil.inflate(
        layoutInflater, R.layout.fragment_book_list, container, false
    )

    override fun init() {
        initLoadingDialog()
        initAdapterAndRv()
        fetchRepoList()
    }

    private fun initAdapterAndRv() {
        bookListAdapter = BookListAdapter(onRepoClicked = this::onRepoClicked)
        binding.rvList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = bookListAdapter
        }
    }

    private fun onRepoClicked(repo: BookInfo?) {

        findNavController().navigate(
            R.id.action_bookListFragment_to_bookDetailsFragment,
            bundleOf(
                "book_details" to repo?.url
            )
        )

    }

    private fun fetchRepoList() {
        Timber.d("fetchRepoList: called")

        lifecycleScope.launch {
            viewModel.getBookList().collect {
                when (it) {
                    is Resource.Error -> {
                        dismissLoadingDialog()
                        activity?.snackBarError("${it.message}")
                        Timber.d("error: ${it.message}")
                    }

                    is Resource.Loading -> {
                        showLoadingDialog()
                    }

                    is Resource.NetworkError -> {
                        dismissLoadingDialog()
                        activity?.snackBarError("${it.message}")
                    }

                    is Resource.SessionExpired -> {
                        dismissLoadingDialog()
                    }

                    is Resource.Success -> {
                        dismissLoadingDialog()
                        val data = it.data
                        if (!data?.data.isNullOrEmpty()) {

                            bookListAdapter?.setValue(data?.data)
                        }
                    }

                }
            }
        }
    }

}
