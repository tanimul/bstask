package com.example.bstask.extentions

import com.example.bstask.common.extention.checkIsEmpty
import junit.framework.TestCase.assertFalse
import junit.framework.TestCase.assertTrue
import org.junit.Test

class ExtTest {

    @Test
    fun checkIsEmpty() {
        assertTrue("".checkIsEmpty())
        assertTrue("null".checkIsEmpty())
        assertFalse("hello".checkIsEmpty())
    }

}