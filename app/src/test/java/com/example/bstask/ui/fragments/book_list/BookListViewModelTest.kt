package com.example.bstask.ui.fragments.book_list

import com.example.bstask.models.BookInfo
import com.example.bstask.models.BookList
import com.example.bstask.network.core.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class BookListViewModelTest {

    @Mock
    private lateinit var viewModel: BookListViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun fetchBookList_Success() = runTest {
//        val testDataFlow = BookList(
//            status = 200,
//            message = "Request success",
//            data = arrayListOf(
//                BookInfo(
//                    101,
//                    "Programming Language C",
//                    "https://picsum.photos/id/1/300/300",
//                    "https://en.wikipedia.org/wiki/C_(programming_language)"
//                ),
//            )
//        )
//        val resource = Resource.Success(testDataFlow)
//        val flow: Flow<Resource<BookList>> = flowOf(resource)
//
//        `when`(viewModel.getBookList()).thenReturn(flow)
//
//        val result = viewModel.getBookList().first()
//        assertEquals(testDataFlow, result)
    }

}