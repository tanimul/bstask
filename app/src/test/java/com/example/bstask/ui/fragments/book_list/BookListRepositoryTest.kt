package com.example.bstask.ui.fragments.book_list

import android.net.ConnectivityManager
import com.example.bstask.models.BookInfo
import com.example.bstask.models.BookList
import com.example.bstask.network.api.BookApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class BookListRepositoryTest {

    @Mock
    private lateinit var bookApi: BookApi

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun getBookList_Success() = runTest {
        val bookList = Response.success(BookList())
        Mockito.`when`(bookApi.getBookList()).thenReturn(bookList)

        val result = bookApi.getBookList()
        assertEquals(result,bookList)
    }

    @Test
    fun getBookList_Error() = runTest {
        val bookList = Response.error<BookList>(
            404,
            "Error message".toResponseBody("text/plain".toMediaTypeOrNull())
        )

        Mockito.`when`(bookApi.getBookList()).thenReturn(bookList)

        val result = bookApi.getBookList()
        assertEquals(result,bookList)
    }
}